const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let orderSchema = new Schema({
	_id: Schema.Types.ObjectId,
	userId: { type: Schema.Types.ObjectId, ref: 'user' },
	subTotal: { type: Number, default: 0 },
	date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('order', orderSchema);
