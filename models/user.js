const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let userSchema = new Schema({
	_id: Schema.Types.ObjectId,
	name: { type: String, required: true },
	noOfOrders: { type: Number, default: 0 },
});

module.exports = mongoose.model('user', userSchema);
