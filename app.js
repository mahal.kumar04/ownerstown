// import required packages
const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');

// import required routes
const user = require('./routes/user');
const order = require('./routes/order');

// app initialization
const app = express();

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// change the mongo connection url to your own
mongoose.connect('mongodb://127.0.0.1:27017/ownerstown', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

// calling routes
app.use('/user', user);
app.use('/order', order);

app.use('/', (req, res, next) => {
	res.send('server is running');
});

app.use('*', function (req, res) {
	res.send('what???', 404);
});
app.listen(3000);
