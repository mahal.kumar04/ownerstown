const express = require('express');
const router = express.Router();
const Order = require('../models/order');
const mongoose = require('mongoose');
const moment = require('moment');

// Get all orders
router.get('/', (req, res, next) => {
	Order.find()
		.exec()
		.then((result) => {
			if (result.length == 0) {
				res.status(404).json({
					error: 'No data is availble',
				});
			} else {
				res.status(200).json({
					data: result,
				});
			}
		})
		.catch((err) => {
			res.status(500).json({
				error: 'some error occur',
			});
		});
});

router.post('/addorders/', (req, res, next) => {
	let order = new Order({
		_id: new mongoose.Types.ObjectId(),
		userId: req.body.userId,
		subTotal: req.body.subTotal,
		date: new Date(moment(req.body.date).format('YYYY-MM-DD')),
	});
	order
		.save()
		.then((doc) => {
			res.status(200).json({
				Message: 'order Successfuly Created',
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				err,
			});
		});
});

router.get('/calcavg/', (req, res, next) => {
	Order.aggregate([
		{
			$group: {
				_id: '$userId',
				noOfOrder: { $sum: 1 },
				avgBillValue: { $avg: '$subTotal' },
			},
		},
		{
			$lookup: {
				from: 'users',
				localField: '_id',
				foreignField: '_id',
				as: 'user_data',
			},
		},
		{ $unwind: '$user_data' },

		{
			$project: {
				userId: '$user_data._id',
				name: '$user_data.name',
				noOfOrder: '$noOfOrder',
				averageBillValue: '$avgBillValue',
			},
		},
	])

		.exec()
		.then((result) => {
			res.status(200).json({
				data: result,
			});
		});
});

module.exports = router;
