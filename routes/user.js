const express = require('express');
const router = express.Router();
const User = require('../models/user');
const mongoose = require('mongoose');

router.get('/', (req, res, next) => {
	User.find()
		.exec()
		.then((result) => {
			if (result.length == 0) {
				res.status(404).json({
					error: 'No data is available',
				});
			} else {
				res.status(200).json({
					data: result,
				});
			}
		});
});

router.post('/adduser/', (req, res, next) => {
	User.find({ name: req.body.name })
		.exec()
		.then((result) => {
			if (result.length >= 1) {
				return res.status(401).json({
					error: 'user already exists',
				});
			} else {
				let user = new User({
					_id: new mongoose.Types.ObjectId(),
					name: req.body.name,
				});
				user.save()
					.then((doc) => {
						res.status(200).json({
							Message: 'User Successfuly Created',
						});
					})
					.catch((err) => {
						console.log(err);
						res.status(500).json({
							err,
						});
					});
			}
		})
		.catch((err) => {
			res.status(500).json({
				error: 'some error occur',
			});
		});
});

router.put('/updateuser', (req, res, next) => {
	console.log(req.body.id);
	console.log(req.body.noOfOrders);
	User.findByIdAndUpdate(req.body.id, {
		$set: { noOfOrders: req.body.noOfOrders },
	})
		.then((result) => {
			console.log(result);
			res.status(200).json({
				success: true,
				message: 'Successfully updated',
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(500).json({
				error: 'Some Erorr occur',
			});
		});
});

module.exports = router;
