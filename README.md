# owenerstown assignment

## clone the project

```bash
git clone https://gitlab.com/mahal.kumar04/ownerstown.git
```

## Install packages

Inside directory into the cloned directory

```bash
npm install
```

## Run the project

```bash
npm start
```

or

```bash
npm run dev
```

## Usage

### change mongo url
1. Change the mongo url to your in app.js

### create new users

1. use the /user/adduser/ api to create new user 
    i. api schema- {"name": "USERNAME"}

### create new orders

1. use the /order/addorders/ api to create new order
    i. api schema- {"userId": "id from user table","subTotal": "subtotal amount","date":"dateobject in YYYY-MM-DD format"}

### see avg result

1. use the /order/calcavg/ api to see avg report
    i. no schema required use basic get method

### update user noOfOrders
1. use the /user/updateuser/ api to update user details
    i api schema- {"id":"userid","noOfOrders":"no of orders"}


